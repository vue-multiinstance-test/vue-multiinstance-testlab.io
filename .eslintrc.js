// eslint-disable-next-line @typescript-eslint/no-var-requires
const { resolve } = require('path');

module.exports = {
  root: true,

  parserOptions: {
    extraFileExtensions: ['.vue'],
    parser: '@typescript-eslint/parser',
  },

  env: {
    browser: true,
  },

  globals: {
    process: true,
  },

  plugins: [
    '@typescript-eslint',
    'import',
    'simple-import-sort',
    'jsdoc',
    'sonarjs',
    'vue',
    'prettier',
  ],

  // Rules order is important, please avoid shuffling them
  extends: [
    // typescript
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    // 'plugin:@typescript-eslint/recommended-requiring-type-checking',

    // import
    'plugin:import/recommended',

    // jsdoc
    'plugin:jsdoc/recommended',

    // sonarjs
    'plugin:sonarjs/recommended',

    // vue
    'plugin:vue/recommended',

    // prettier
    'plugin:prettier/recommended',
    'prettier/vue',
    'prettier/@typescript-eslint',
  ],

  rules: {
    'no-param-reassign': 'off',
    camelcase: 'off',
    'prefer-promise-reject-errors': 'off',
    'class-methods-use-this': 'off',
    'global-require': 'off',
    'no-return-assign': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-console': 'off',

    // prettier
    'prettier/prettier': 'error',

    // import
    'import/first': 'off',
    'import/named': 'error',
    'import/namespace': 'error',
    'import/default': 'error',
    'import/export': 'error',
    'import/extensions': 'off',
    'import/no-unresolved': 'off',
    'import/no-extraneous-dependencies': 'off',
    'import/prefer-default-export': 'off',

    // simple-import-sort
    'simple-import-sort/sort': 'error',

    /// jsdoc
    'jsdoc/no-undefined-types': 'off',
    'jsdoc/require-param-type': 'off',
    'jsdoc/require-param-description': 'off',
    'jsdoc/require-returns': 'off',
    'jsdoc/require-returns-type': 'off',
    'jsdoc/require-returns-description': 'off',
    'jsdoc/require-jsdoc': [
      1,
      {
        require: {
          ClassDeclaration: false,
          ClassExpression: false,
          FunctionDeclaration: true,
          FunctionExpression: false,
          MethodDefinition: false,
        },
      },
    ],

    // sonarjs
    'sonarjs/no-duplicate-string': 'off',

    // typescript
    quotes: ['warn', 'single'],
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/naming-convention': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-unused-vars': [2, { args: 'none' }],
    '@typescript-eslint/member-ordering': [
      2,
      {
        default: [
          'public-static-field',
          'protected-static-field',
          'private-static-field',

          'public-instance-field',
          'protected-instance-field',
          'private-instance-field',

          'public-field',
          'protected-field',
          'private-field',

          'static-field',
          'instance-field',

          'field',

          'constructor',

          'public-static-method',
          'protected-static-method',
          'private-static-method',

          'public-instance-method',
          'protected-instance-method',
          'private-instance-method',

          'public-method',
          'protected-method',
          'private-method',

          'static-method',
          'instance-method',

          'method',
        ],
      },
    ],

    // vue
    'vue/component-tags-order': ['error'],
    'vue/no-deprecated-scope-attribute': ['error'],
    'vue/no-deprecated-slot-attribute': ['warn'],
    'vue/no-deprecated-slot-scope-attribute': ['error'],
    'vue/match-component-file-name': ['error'],
    'vue/no-reserved-component-names': ['error'],
    'vue/no-unsupported-features': ['error'],
    'vue/static-class-names-order': 'off',
    'vue/valid-v-bind-sync': ['error'],
    'vue/valid-v-slot': ['error'],
    'vue/eqeqeq': ['error'],
    'vue/no-irregular-whitespace': ['error'],
    'vue/no-unused-components': ['error'],
    'vue/component-name-in-template-casing': [
      'error',
      'kebab-case',
      {
        registeredComponentsOnly: false,
        ignores: [],
      },
    ],
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'any',
          normal: 'never',
          component: 'never',
        },
        svg: 'always',
        math: 'always',
      },
    ],
    'vue/no-v-html': 'off',
  },

  settings: {
    'import/resolver': {
      alias: {
        map: [['src', resolve(__dirname, 'src')]],
      },
    },
  },

  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],
};
