/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');

const env = require('dotenv').config({
  path: path.resolve(__dirname, '.env'),
}).parsed;

// const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

module.exports = {
  transpileDependencies: ['vuetify'],

  configureWebpack: {
    // plugins: [new VuetifyLoaderPlugin()],
  },

  devServer: {
    proxy: env.SPA_PROXY,
  },

  css: {
    sourceMap: !process.env.NODE_ENV === 'production',
    loaderOptions: {
      scss: {
        additionalData: '@import "~@/assets/styles/media.scss";',
      },
    },
    extract: false,
  },

  productionSourceMap: false,

  filenameHashing: false,

  chainWebpack: (config) => {
    config.plugins.delete('html');
    config.plugins.delete('preload');
    config.plugins.delete('prefetch');
  },

  publicPath:
    process.env.NODE_ENV === 'production'
      ? 'https://vue-multiinstance-test.gitlab.io/'
      : '/',

  pages: {
    index: {
      entry: 'src/main.ts',
      template: 'public/index.html',
      filename: 'index.html',
    },
    checkout: {
      entry: 'src/main.ts',
      template: 'public/checkout.html',
      filename: 'checkout.html',
    },
  },
};
