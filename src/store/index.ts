import Vue from 'vue';
import Vuex from 'vuex';

import { SET } from '@/store/mutations';

import { emailVerification } from './modules';
import { PersistedStatePlugin } from './plugins';

Vue.use(Vuex);

/**
 * Корневое состояние приложения
 */
interface IAppState {
  /**
   * Открыто ли выпадающее меню
   */
  isMenuOpen: boolean;
}

const state = (): IAppState => ({
  isMenuOpen: false,
});

export default new Vuex.Store({
  strict: process.env.NODE_ENV === 'development',

  state,

  mutations: {
    SET_IS_MENU_OPEN: SET('isMenuOpen'),
  },

  actions: {
    /**
     * Меняет состояние открыто/закрыто у выпадающего меню
     *
     * @param context
     * @param context.commit
     * @param isMenuOpen Открыто ли меню
     */
    changeMenuState({ commit }, isMenuOpen: IAppState['isMenuOpen']) {
      commit('SET_IS_MENU_OPEN', isMenuOpen);
    },
  },

  plugins: [PersistedStatePlugin],

  modules: { emailVerification },
});
