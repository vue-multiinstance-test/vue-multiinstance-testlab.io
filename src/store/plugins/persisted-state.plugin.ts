import createPersistedState from 'vuex-persistedstate';

/**
 * Плагин для синхронизации состояния приложения с localStorage
 */
const ersistedStatePlugin = createPersistedState({
  paths: ['emailVerification'],
});

export default ersistedStatePlugin;
