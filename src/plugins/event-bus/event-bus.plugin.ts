import Vue from 'vue';

Vue.prototype.$eventBus = new Vue();

declare module 'vue/types/vue' {
  interface Vue {
    /**
     * Шина событий
     */
    $eventBus: {
      /**
       * Устанавливает слушатель на событие
       */
      $on: (actionName: string, callback: (value?: any) => void) => void;

      /**
       * Удаляет слушатель на событие
       */
      $off: (actionName: string, callback: (value?: any) => void) => void;

      /**
       * Вызывает событие
       */
      $emit: (actionName: string, value?: any) => void;
    };
  }
}
