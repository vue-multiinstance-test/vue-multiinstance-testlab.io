import Vue from 'vue';

import * as apiEntities from '@/api';

declare module 'vue/types/vue' {
  interface Vue {
    /**
     * Плагин для работы с API
     */
    $api: typeof apiEntities;
  }
}

Vue.prototype.$api = apiEntities;
