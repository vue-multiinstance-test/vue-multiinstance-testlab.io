import './styles';

import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: '#072caf',
        secondary: '#989eb7',
        accent: '#081b5f',
        error: '#ff6f79',
        info: '#37c5d3',
        success: '#75c663',
        warning: '#ffc672',
      },
    },
  },
});
