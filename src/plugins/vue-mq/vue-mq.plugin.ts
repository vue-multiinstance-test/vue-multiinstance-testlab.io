import Vue from 'vue';
import VueMq from 'vue-mq';

Vue.use(VueMq, {
  breakpoints: {
    'mobile-s': 320,
    'mobile-m': 375,
    'mobile-l': 425,
    tablet: 768,
    laptop: 1024,
    'laptop-l': 1440,
    'laptop-xl': 2559,
    'laptop-4k': Infinity,
  },
  defaultBreakpoint: 'laptop-l',
});
