import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import Vuelidate from 'vuelidate';

Component.registerHooks(['validations']);

Vue.use(Vuelidate);
