import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import Vue from 'vue';

import router from '@/router';

declare module 'vue/types/vue' {
  interface Vue {
    $axios: AxiosInstance;
  }
}

export { AxiosRequestConfig, AxiosResponse };

/**
 * Кастомный инстанс axios
 */
export const $axios = axios.create({
  headers: {
    'Content-Type': 'application/json',
  },
});

/**
 * Pre-request hook
 */
$axios.interceptors.request.use(
  (config: AxiosRequestConfig) => config,
  (error) => {
    alert('err');
    return Promise.reject(error);
  },
);

/**
 * Pre-response hook
 */
$axios.interceptors.response.use(
  (response: AxiosResponse) => {
    return response;
  },
  (error) => {
    const { status } = error.response;

    if (status === 401) {
      router.replace({
        name: 'login',
      });
    }

    return Promise.reject(error);
  },
);

Vue.prototype.$axios = $axios;
