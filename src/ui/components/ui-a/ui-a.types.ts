/**
 * Тэг ссылки
 */
export type UiATag = 'a' | 'router-link' | 'button';
