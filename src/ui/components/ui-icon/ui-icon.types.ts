/**
 * Доступные имена иконок
 */
export type UiIconNames =
  | 'account'
  | 'account-2'
  | 'arrow-pointer'
  | 'arrow-pointer-2'
  | 'bank'
  | 'calendar'
  | 'chevron-down'
  | 'close'
  | 'copy'
  | 'desk'
  | 'email'
  | 'error'
  | 'expired'
  | 'eye-active'
  | 'eye-disabled'
  | 'gear'
  | 'globe'
  | 'graph'
  | 'order-direction'
  | 'periscope'
  | 'pointer-down'
  | 'pointer-right'
  | 'pointer-up'
  | 'print'
  | 'refresh'
  | 'sand-timer'
  | 'save'
  | 'shield'
  | 'success'
  | 'transaction-daily-limit'
  | 'transaction-monthly-limit'
  | 'user-avatar'
  | 'warning';
