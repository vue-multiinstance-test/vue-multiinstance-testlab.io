/**
 * Размер кнопки
 */
export type UiButtonSize = 'small' | 'standart' | 'large';

/**
 * Тэг кнопки
 */
export type UiButtonTag = 'button' | 'a';

/**
 * Цвет кнопки
 *
 * Не использует переменные, доступны только имена цветов у которых есть
 * hover/pressed состояния
 */
export type UiButtonColor =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'light'
  | 'dark'
  | 'link';
