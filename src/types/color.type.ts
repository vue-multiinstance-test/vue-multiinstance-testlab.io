/**
 * Цвет
 *
 * Имена цветов используются в css переменных, доступны только имена у которых есть
 * hover/pressed состояния
 */
export type Color =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'light'
  | 'dark'
  | 'link';
