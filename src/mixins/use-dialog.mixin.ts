import { Component, Prop, Vue } from 'vue-property-decorator';

/**
 * Миксин диалога
 */
@Component
export default class UseDialog extends Vue {
  /**
   * Видимость диалога
   */
  @Prop({ type: Boolean, default: false })
  readonly value!: boolean;

  /**
   * Дефолтные параметры диалога
   */
  defaultOptions = {
    'max-width': 448,
    width: 'calc(100% - 2rem)',
    transition: 'slide-y-reverse-transition',
    'overlay-color': '#261956',
    'overlay-opacity': 0.7,
  };
}
