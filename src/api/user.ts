import { $axios } from '@/plugins/axios';

/**
 * Получение информации о пользователе
 */
export async function getUser() {
  return await $axios.get('/spa/profile');
}
