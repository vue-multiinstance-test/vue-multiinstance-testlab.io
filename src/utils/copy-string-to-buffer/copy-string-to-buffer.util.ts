/**
 * Копирование строки в буффер обмена
 *
 * @param string То что нужно скопировать
 */
export default function copyStringToBuffer(string: string): void {
  const tempInput = document.createElement('INPUT') as HTMLInputElement;
  const { activeElement }: any = document;
  tempInput.value = string;
  document.body.appendChild(tempInput);
  tempInput.select();
  document.execCommand('copy');
  document.body.removeChild(tempInput);
  // eslint-disable-next-line no-unused-expressions
  activeElement?.focus?.();
}
