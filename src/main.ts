import '@/assets/styles/index.scss';
import '@/ui';
import '@/plugins';
import 'vue-class-component/hooks';

import Vue, { VueConstructor } from 'vue';

import AppCheckout from '@/apps/app-checkout';
import AppCountdownTimer from '@/apps/app-countdown-timer';
import vuetify from '@/plugins/vuetify';
import router from '@/router';
import store from '@/store';
import { nanoid } from '@/utils';

Vue.config.productionTip = false;
Vue.config.devtools = process.env.NODE_ENV === 'development';

/**
 * Генерирует конфигурацию приложения
 *
 * @param app
 */
function configureApp(app) {
  return {
    router,
    store,
    vuetify,
    render: (h) => h(app),
  };
}

/**
 * Монтирует доступные приложения
 */
function mountApps(): void {
  const appsMap: Record<string, VueConstructor<Vue>> = {
    'app-checkout': AppCheckout,
    'app-countdown-timer': AppCountdownTimer,
  };

  Object.keys(appsMap).forEach((tagName) => {
    Array.from(document.querySelectorAll(tagName)).forEach((app) => {
      const uid = `SPA-${nanoid()}`;
      app.id = uid;

      new Vue(configureApp(appsMap[tagName])).$mount(`#${uid}`);
    });
  });
}

mountApps();
