$( document ).ready(function() {
  $('.b-mob-nav').click(function() {
    if (!($(this).hasClass('active'))) {
      $(this).addClass('active');
      $('body').addClass('b-mobv-nav-active');
      return false;

    } else {
      $(this).removeClass('active');
      $('body').removeClass('b-mobv-nav-active');
      return false;
    }
  });

  $('body').on('click', '.b-popup-mob-nav-splash', function() {
    $('.b-mob-nav').removeClass('active');
    $('body').removeClass('b-mobv-nav-active');
    return false;
  });

  var $page = $('html, body');
  $('.btn-to-top').click(function() {
      $page.animate({
          scrollTop: $($.attr(this, 'href')).offset().top
      }, 400);
      return false;
  });

  $('.scroll-pane').jScrollPane({
    autoReinitialise: true,
    verticalDragMinHeight: 0,
    verticalDragMaxHeight: 110
  });

  $('.m-slider').slick({
    dots: true,
	arrows: true,
  autoplay: true,
    nextArrow: '.m-next',
    prevArrow: '.m-prev',
    responsive: [
  		{
  		  breakpoint: 767,
  		  settings: {
    			dots: false
  		  }
  		}
	  ]
  });

  $('.popular-slider').slick({
		fade: false,
		slidesToShow: 4,
		variableWidth: false,
		slidesToScroll: 1,
        centerPadding: 10,
		dots: true,
		arrows: true,
		autoplay: true,
		responsive: [
  		{
  		  breakpoint: 1100,
  		  settings: {
  			slidesToShow: 3
  		  }
  		},
  		{
  		  breakpoint: 992,
  		  settings: {
  			slidesToShow: 2
  		  }
  		},
  		{
  		  breakpoint: 568,
  		  settings: {
  			slidesToShow: 1
  		  }
  		}
	  ]
	});
	
	/* dropdown hover add */
    function toggleDropdown (e) {
    	const _d = $(e.target).closest('.dropdown').first(),
    		_m = $('.dropdown-menu:first', _d).first();
    	setTimeout(function(){
    		const shouldOpen = e.type !== 'click' && _d.is(':hover');
    		_m.toggleClass('show', shouldOpen);
    		_d.toggleClass('show', shouldOpen);
    		$('[data-toggle="dropdown"]', _d).first().attr('aria-expanded', shouldOpen);
    	}, e.type === 'mouseleave' ? 200 : 200);
    }
    
    $('body')
    .on('mouseenter mouseleave','.catalog-navi-item',toggleDropdown)
    .on('click', '.catalog-navi-link', toggleDropdown);

    $('.catalog-navi-list .catalog-navi-link-d').on("click", function(e){
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });
});

// for section catalog
let btnCatalogAll = document.querySelector(".catalog-more-all");
let btnCatalogMain = document.querySelector(".catalog-more-main");
let catalogAllItems = document.querySelector(".catalog-all-items");
let catalogDropdown = document.querySelector(".catalog-dropdown");

let catalogItems = document.querySelectorAll(".catalog-all-items [class^='col-']");

function clickBtn(event) {
  function x() {
    catalogAllItems.classList.add("d-height");
    catalogDropdown.classList.remove("animate__fadeOutDown");
    catalogDropdown.classList.add("opened").classList.add("animate__fadeInUp");
  }
  setTimeout(x, 300);

  btnCatalogAll.classList.add("d-none");
  btnCatalogMain.classList.add("d-block");

  catalogItems[0].classList.add("animate__fadeOutLeft");
  catalogItems[1].classList.add("animate__fadeOutLeft");
  catalogItems[0].classList.remove("animate__fadeInLeft");
  catalogItems[1].classList.remove("animate__fadeInLeft");
  catalogItems[2].classList.add("animate__fadeOutRight");
  catalogItems[3].classList.add("animate__fadeOutRight");
  catalogItems[2].classList.remove("animate__fadeInRight");
  catalogItems[3].classList.remove("animate__fadeInRight");
}

function clickBtnBack(event) {
  function x() {
    catalogAllItems.classList.remove("d-height");
    catalogDropdown.classList.remove("opened").classList.remove("animate__fadeInUp");
    catalogDropdown.classList.add("animate__fadeOutDown");
  }
  setTimeout(x, 0);

  btnCatalogAll.classList.remove("d-none");
  btnCatalogMain.classList.remove("d-block");

  catalogItems[0].classList.remove("animate__fadeOutLeft");
  catalogItems[1].classList.remove("animate__fadeOutLeft");
  catalogItems[0].classList.add("animate__fadeInLeft");
  catalogItems[1].classList.add("animate__fadeInLeft");
  catalogItems[2].classList.remove("animate__fadeOutRight");
  catalogItems[3].classList.remove("animate__fadeOutRight");
  catalogItems[2].classList.add("animate__fadeInRight");
  catalogItems[3].classList.add("animate__fadeInRight");
}

// setEventListener
function setEventListener(el, f) {
  if(el) {
    el.addEventListener('click', f)
  }
}
setEventListener(btnCatalogAll, clickBtn);
setEventListener(btnCatalogMain, clickBtnBack);
